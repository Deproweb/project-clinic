import { useContext } from 'react';
import { citesContext } from '../../App';

import  './Mycites.scss';

export const Mycites = () => {

    const {cites} = useContext(citesContext)

    console.log('Estas son las citas', cites)

    return (<div className="c-mycites">

        {!cites.length && <div className="c-mycites__noinfo">
            <h4>No hay citas</h4>
            <p>Prueba a ir al apartado de pedir citas y programa una</p>
            <p>También puedes solcitar una cita telefónica o por chat</p>
        </div>}

        {cites.map(element => 
            <div key={element.DNI} className="c-mycites__info">
                <h5>Dni: {element.DNI}</h5>
                <h5>Fecha: {element.Fecha}</h5>
                <h5>Especialidad: {element.Especialidad}</h5>
            </div>
        )}
    
    </div>)
}

import './Presentation.scss'

//Boostrap
import Accordion from 'react-bootstrap/Accordion'

export const Presentation = () => {
    

    return (<div>
        <div className="c-presentation">
        <div className="c-presentation__title">
            <h3>Bienvenido a la Clínica Centro</h3>
        </div>
            <div className="c-presentation__info">
                <img src="https://cdn.pixabay.com/photo/2014/12/10/20/56/medical-563427_1280.jpg" alt="foto_clinica" />
            </div>
        </div>
        <Accordion>
            <Accordion.Item eventKey="0">
                <Accordion.Header>¿Quiénes somos?</Accordion.Header>
                <Accordion.Body>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
                est laborum.
                </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="1">
                <Accordion.Header>¿Cómo trabajamos?</Accordion.Header>
                <Accordion.Body>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
                est laborum.
                </Accordion.Body>
            </Accordion.Item>
        </Accordion>
    </div>)
}

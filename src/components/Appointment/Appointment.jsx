import './Appointment.scss';

//Icons
import { BsFillTelephoneFill, BsFillCalendarDateFill, BsFillChatDotsFill } from 'react-icons/bs';

//Boostrap
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'

//Hooks
import { useContext, useState } from 'react';

//Context
import { citesContext } from '../../App';

//Hook-form
import { useForm } from "react-hook-form";


export const Appointment = () => {

    const { register, handleSubmit} = useForm();
    
    const {cites, setCites} = useContext(citesContext);
    
    const [showCites, setShowCites] = useState(false);

    const [showTelephone, setShowTelephone] = useState(false);

    const [showChat, setShowChat] = useState(false)

    const onSubmit = (data, ev) => {
        
        console.log(data);
        
        const pushin = [...cites, data];

        setCites(pushin)

        ev.target.reset()

    };

    return (<div className="c-appointment">

        <div className="c-appointment__inmediatly">
            <h4><BsFillTelephoneFill/></h4>
            <h5>CONSULTA TELEFÓNICA INMEDIATA</h5>
            <p>Resuelve tus dudas rapidamente a través de nuestro equipo de profesionales</p>
            <Button onClick={()=>setShowTelephone(!showTelephone)} variant="warning">Solicitar llamada</Button>
            {showTelephone && <div className="modal__telephone">
                <Modal.Dialog>
                    <Modal.Header >
                        <Modal.Title>Solicitar llamada</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <p>Pon tú número de teléfono y te llamaremos</p>
                        <input type="text" />
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="primary">Solicitar</Button>
                    </Modal.Footer>
                </Modal.Dialog>
            </div>}
        </div>
        <div className="c-appointment__inmediatly">
            <h4><BsFillCalendarDateFill/></h4>
            <h5>PROGRAMA UNA CITA PRESENCIAL</h5>
            <p>Podrás elegir la fecha en la que venir a nuestras instalaciones </p>
            <Button onClick={()=>{setShowCites(!showCites)}} variant="warning">Programar cita</Button>
            {showCites && <div className="c-appointment__inmediatly__program">
                    
                <h4>¿Cuándo deseas la cita?</h4>

                    <form className="c-appointment__form" onSubmit={handleSubmit(onSubmit)}>
        
                        <input className="c-appointment__form__input" placeholder="DNI" {...register("DNI")} />
                        
                        <select className="c-appointment__form__input" {...register("Especialidad")}>
                            <option value="Chequeo Médico">Chequeo Médico</option>
                            <option value="Medicina Familiar">Medicina Familiar</option>
                            <option value="Medicina Estética">Medicina Estética</option>
                            <option value="Medicina Especialista">Medicina Especialista</option>
                        </select>
                        
                        <input className="c-appointment__form__input" type="date" {...register("Fecha")} />    

                        <input className="c-appointment__form__button" type="submit" />

                    </form>
            </div>}
        </div>
        <div className="c-appointment__inmediatly">
            <h4><BsFillChatDotsFill/></h4>
            <h5>CITA POR CHAT</h5>
            <p>Si lo prefieres, puedes realizar una cita por chat con unos de nuestros profesionales</p>
            <Button onClick={()=>{setShowChat(!showChat)}} variant="warning">Abrir chat</Button>
            {showChat && <div className="modal__telephone">
                <Modal.Dialog>
                    <Modal.Header >
                        <Modal.Title>Solicitar chat</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <p>Pon tú número de teléfono y te hablaremos</p>
                        <input type="text" />
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="primary">Solicitar</Button>
                    </Modal.Footer>
                </Modal.Dialog>
            </div>}
        </div>
    </div>)
}

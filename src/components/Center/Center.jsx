import './Center.scss'

export const Center = () => {
    return (<div className="c-center">

        <h4>Nuestro centro</h4>

        <img src="https://img.freepik.com/foto-gratis/blur-hospital_1203-7972.jpg" alt="clinica" />

        <div className="c-center__center">
            <h5>Dirección: Calle Granada, n. 12</h5>
            <p>50 camas de hospitalización, 22 habitaciones y 1 suite</p>
            <p>3 quirófanos</p>
            <p>2 puestos de reanimación y un box de resucitaciónpuestos de reanimación y un box de resucitación</p>
            <p>2 paritorios. Bañera para la asistencia de partos naturales</p>
            <p>7 camas de UCI adultos y 9 camas de UCI neonatal</p>
            <p>16 consultas externas</p>
        </div>

    </div>)
}
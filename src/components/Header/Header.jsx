import './Header.scss'

//Boostrap
import Nav from 'react-bootstrap/Nav'

//Router-dom
import { Link } from 'react-router-dom'

export const Header = () => {

    return (<div className="c-header">
        <img className="c-header__logo" src="https://cdn.discordapp.com/attachments/846417268506099735/942798664265510922/Logotipo_personalizable_letra_inicial_abstracto.png" alt="logotipo" />
        <div className="c-header__nav">
            <Nav className="c-header__nav" activeKey="/home">
                <Nav.Item>
                    <Link to="/" className="c-header__nav__link">Inicio</Link> 
                </Nav.Item>
                <Nav.Item>
                    <Link to="/clinic" className="c-header__nav__link">La clínica</Link> 
                </Nav.Item>
                <Nav.Item>
                        <Link to="/workers" className="c-header__nav__link">Profesionales</Link>  
                </Nav.Item>                           
                <Nav.Item>
                    <Link to="/center" className="c-header__nav__link">Nuestro Centro</Link> 
                </Nav.Item>
            </Nav>
        </div>
    </div>)
}

import './Services.scss'

//Icons
import {MdExpandMore} from 'react-icons/md'

//Hooks
import { useState } from 'react'

export const Services = () => {

    const [showMoreInfo, setShowMoreInfo] = useState(false)
    const [showMoreInfo2, setShowMoreInfo2] = useState(false)
    const [showMoreInfo3, setShowMoreInfo3] = useState(false)
    const [showMoreInfo4, setShowMoreInfo4] = useState(false)



    return (<div className="c-services">
        <div className="c-services__logo">
            <img src="https://cdn.pixabay.com/photo/2019/09/28/10/38/medical-4510408_1280.png" alt="logo" />
        </div>
        <div onClick={()=>setShowMoreInfo(!showMoreInfo)} className="c-services__card">
            <h5 >Chequeos Médicos</h5>
            <h5><MdExpandMore/></h5>
            {showMoreInfo && <div>
                <p>Prueba Covid-19</p>
                <p>Análisis de Sangre u Orina</p>
                <p>Radiología</p>    
            </div>}    
        </div>
        <div onClick={()=>setShowMoreInfo2(!showMoreInfo2)} className="c-services__card">
            <h5 >Medicina Familiar</h5>
            <h5><MdExpandMore/></h5>
            {showMoreInfo2 && <div>
                <p>Médico de familia</p>
                <p>Pediatría</p>
                <p>Enfermeria</p>    
            </div>}    
        </div>
        <div onClick={()=>setShowMoreInfo3(!showMoreInfo3)} className="c-services__card">
            <h5 >Medicina estética</h5>
            <h5><MdExpandMore/></h5>
            {showMoreInfo3 && <div>
                <p>Cirugía maxilofacial</p>
                <p>Control de peso </p>
                <p>Implantes</p>    
            </div>}    
        </div>
        <div onClick={()=>setShowMoreInfo4(!showMoreInfo4)} className="c-services__card">
            <h5 >Medicina especialista</h5>
            <h5><MdExpandMore/></h5>
            {showMoreInfo4 && <div>
                <p>Traumatología</p>
                <p>Cardiología</p>
                <p>Obstetricia</p>    
            </div>}    
        </div>
    </div>)
}

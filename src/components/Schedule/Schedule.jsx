import './Schedule.scss'

import { GrSchedule } from 'react-icons/gr';
import {MdSchedule, MdScheduleSend} from 'react-icons/md'

export const Schedule = () => {
    return (<div className="c-schedule">
        <div className="c-schedule__div">
            <h4><MdSchedule/></h4>
            <h5>Atención al Paciente</h5>
            <p>La Atención al Paciente se encuentra en el Hall Principal. El horario de atención es de Lunes a Viernes de 9:00 a 19:00 horas.</p>
        </div>
        <div className="c-schedule__div">
            <h4><GrSchedule/></h4>
            <h5>Visitas</h5>
            <p>El horario de las visitas a los pacientes es libre, excepto en los casos en que exista contraindicación médica. Se recomienda, de todos modos, que no se realicen visitas pasadas las 21 horas</p>
        </div>
        <div className="c-schedule__div">
            <h4><MdScheduleSend/></h4>
            <h5>UCI</h5>
            <p>La UCI puede recibir la visita de un único familiar a las 13.00 h. y a las 19.00 h.</p>
        </div>
    </div>)
}

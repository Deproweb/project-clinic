import './Contact.scss';

import Button from 'react-bootstrap/Button'
import { useForm } from "react-hook-form";

export const Contact = () => {

    const { register, handleSubmit, formState: { errors } } = useForm();
    const onSubmit = data => console.log(data);

    return (<div className="c-contact">

            <h4>Contacto</h4>
            <h5>Envia tu consulta y te responderemos con la máxima brevedad</h5>

            <form className="c-contact__form" onSubmit={handleSubmit(onSubmit)}>
                <input className="c-contact__form__input" placeholder="Nombre" {...register("nombre")} />
            
                <input className="c-contact__form__input" placeholder="Teléfono" {...register("telefono", { required: true })} />
                {errors.telefono && <span>Este campo es obligatorio</span>}

                <input className="c-contact__form__input" placeholder="Email" {...register("email")} />    

                <input className="c-contact__form__input__text" placeholder="Consulta" {...register("consulta")} /> 

                <Button variant="outline-warning">Enviar</Button>{' '}
            </form>
    </div>)
}
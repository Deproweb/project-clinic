import { useState } from 'react'
import './CenterPage.scss'

//Boostrap
import Button from 'react-bootstrap/Button'
import { Schedule } from '../../components/Schedule/Schedule';
import { Center } from '../../components/Center/Center';
import { Contact } from '../../components/Contact/Contact';

export const CenterPage = () => {

    const [showSchedule, setShowSchedule] = useState(true);
    const [showCenter, setShowCenter] = useState(false);
    const [showContact, setShowContact] = useState(false);

    const scheduleFn = () => {
        setShowSchedule(true)
        setShowCenter(false)
        setShowContact(false)
    };

    const centerFn = () => {
        setShowSchedule(false)
        setShowCenter(true)
        setShowContact(false)
    }

    const contactFn = () => {
        setShowSchedule(false)
        setShowCenter(false)
        setShowContact(true)
    }

    return (<div className="c-center">

        <div className="c-center__buttons">
            <Button onClick={scheduleFn} variant="light">Horarios</Button>{' '}
            <Button onClick={centerFn} variant="light">Centro</Button>{' '}
            <Button onClick={contactFn} variant="light">Contacto</Button>{' '}
        </div>
        <div className="c-center__info">
            {showSchedule && <Schedule/>}
            {showCenter && <div><Center/></div>}
            {showContact && <div><Contact/></div>}
        </div>
    </div>)
}

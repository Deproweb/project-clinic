//Hooks
import { useState } from 'react';

//Boostrap
import Button from 'react-bootstrap/Button'

//Components
import { Appointment } from '../../components/Appointment/Appointment';
import { Mycites } from '../../components/Mycites/Mycites';
import { Services } from '../../components/Services/Services';

//Scss
import './ClinicPage.scss'

export const ClinicPage = () => {
    
    const [services, setServices] = useState(true);
    const [appointment, setAppointment] = useState(false);
    const [citas, setCitas] = useState(false)

    const servicesFn = () => {
        setAppointment(false)
        setCitas(false)
        setServices(true)
    };

    const appointmentFn = () => {
        setAppointment(true)
        setCitas(false)
        setServices(false)
    }

    const citasFn = () => {
        setAppointment(false)
        setCitas(true)
        setServices(false)
    }

    return (<div className="c-clinic">
        
        <div className="c-clinic__buttons">
            <Button onClick={servicesFn} variant="light">Servicios</Button>{' '}
            <Button onClick={appointmentFn} variant="light">Pedir cita</Button>{' '}
            <Button onClick={citasFn} variant="light">Mis citas</Button>{' '}
        </div>

        <div className="c-clinic__info">
            {services && <div>
                <Services/>
            </div>}
            {appointment && <div>
                <Appointment/>
            </div>}
            {citas && <div>
                <Mycites/>
            </div>}
        </div>
    </div>
    )
}

import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { callToApi } from '../../redux/actions/apiActions';
import Card from 'react-bootstrap/Card'
import './WorkerPage.scss'

const WorkerPage = (props) => {
    
    useEffect(() => {
        props.dispatch(callToApi())
          // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    
    

    return (<div className="c-workerpage">
        <div className="c-workerpage__info">
        { props.workers.map(element => 
            <Card key={element.name} style={{ width: '18rem', margin: '15px', border: '2px solid greys' }}>
                <Card.Img variant="top" src={element.img} />
                <Card.Body>
                    <Card.Title>{element.name}</Card.Title>
                    <Card.Text>
                        {element.description}
                    </Card.Text>
                    {/* <Button variant="warning">Conocer</Button> */}
                    <p>Málaga</p>
                </Card.Body>
            </Card>
        )}
        </div>
        </div>)
}

const mapStateToProps = (state) => ({
    workers: state.api.workers
})

export default connect(mapStateToProps)(WorkerPage)
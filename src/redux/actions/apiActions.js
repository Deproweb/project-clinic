import axios from "axios";


export const GET_WORKERS = "[API] getWorkers";


const getWorkers = (data) => ({

    type: GET_WORKERS,

    payload: data

})

export const callToApi = () => {

    return async (dispatch) => {
        
        const res = await axios.get('https://my-json-server.typicode.com/DeproWeb/workers/db')
        const data = res.data.workers
        dispatch(getWorkers(data))
        console.log(data);
        
    }

}
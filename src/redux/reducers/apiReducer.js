import { GET_WORKERS } from "../actions/apiActions";


const INITIAL_STATE = ({
    workers: [],
    error: null
});

export const apiReducer = (state = INITIAL_STATE, action) => {

    switch (action.type) {

        case GET_WORKERS :
            return {
                ...state,
                workers: action.payload
            }


        default:
            return state

    }


}

import React, { useState } from "react";

import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";

import './App.scss';


//Components
import { Header } from "./components/Header/Header";
import { Footer } from "./components/Footer/Footer";

//Pages
import { HomePage } from "./pages/HomePage/HomePage";
import  WorkerPage  from "./pages/WokerPage/WorkerPage";
import { ClinicPage } from "./pages/ClinicPage/ClinicPage";
import { CenterPage } from "./pages/CenterPage/CenterPage";

//UseContext
export const citesContext = React.createContext();

function App() {
  
  const [cites, setCites] = useState([])
  
  const exportValue =  {
    cites,
    setCites
  }

  return (
    <div className="app">
      <citesContext.Provider value={exportValue}>
        <BrowserRouter>
          <Header/>
          <Routes>
            <Route path="/" element={<HomePage />}/> 
            <Route path="clinic" element={<ClinicPage />}/>
            <Route path="workers" element={<WorkerPage />} />
            <Route path="center" element={<CenterPage />} />
          </Routes>
          <Footer/>
        </BrowserRouter>
        </citesContext.Provider>
    </div>
  );
}

export default App;
